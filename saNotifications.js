angular.module('saNotifications', ['ngSanitize']).factory('saNotificationsAPI', function() {
	var notifications = {}
	
	var add = function(params_object){
		if(params_object.bin !== undefined){
			if(notifications[params_object.bin] === undefined){
				notifications[params_object.bin] = []
			}

			var class_name = params_object.alert_type
			class_name += (params_object.block === true)?' alert-block':''

			notifications[params_object.bin].push({
				id:notifications[params_object.bin].length,
				class_name:class_name,
				message:params_object.message
			})
		}
	}
	
    return {
		get:function(bin){
			return notifications[bin]
		},
		show:function(bin){
			return (notifications[bin] !== undefined && notifications[bin].length > 0)?true:false
		},
		alert:function(params_object){
			params_object.alert_type = ''
			add(params_object)
		},
        success: function(params_object) {
			params_object.alert_type = 'alert-success'
			add(params_object)
        },
        error: function(params_object) {
            params_object.alert_type = 'alert-error'
			add(params_object)
        },
		info:function(params_object){
			params_object.alert_type = 'alert-info'
			add(params_object)
		},
		remove:function(bin, id){
			for(var i in notifications[bin]){
				if(notifications[bin][i].id === id){
					notifications[bin].splice(i,1)
				}
			}
		},
	    clear:function(bin){
		    notifications[bin] = [];
	    }
    }
}).directive('saNotifications', function() {
    return {
        restrict: 'E',
        scope: {},
        replace: true,
		link:function(scope, element, attrs){
			scope.bin = attrs.bin
		},
        controller: function($scope, saNotificationsAPI) {
            $scope.api = saNotificationsAPI
        },
        template: '<div ng-show="api.show(bin)" class="sa-notifications">'+
				  '<div ng-repeat="notification in api.get(bin)">'+
				  '<div role="alert" class="alert {{notification.class_name}}">' +
                  '  <button type="button" class="close" ng-click="api.remove(bin, notification.id)">&times;</button>' +
                  '  <span ng-bind-html="notification.message"></span>' +
                  '</div>'+
				  '</div>'+
				  '</div>'
    }
});